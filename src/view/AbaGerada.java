package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.AbaGeradaController;

//Eventualmente usar a l�gica dessa classe para gerar tamb�m objetos de tipo Card gen�ricos em runtime apenas com templates.
public class AbaGerada extends JPanel {
	
	private JButton bCriarHtml;
	private JButton bVoltar;
	private HashMap<String, JTextField> entradas;

	private String nomeCard;

	public AbaGerada(List<String> propriedades, String nomeCard)
	{
		this.setVisible(true);

		this.nomeCard = nomeCard;
		
		ArrayList<JLabel> lbsPropriedades = new ArrayList<>();
		entradas = new HashMap<String, JTextField>();

		for(String propriedade : propriedades) {
			lbsPropriedades.add(new JLabel(propriedade + ":"));
			entradas.put(propriedade, new JTextField());
		}
		
		AbaGeradaController controller = new AbaGeradaController(this);
		
		bVoltar  = new JButton("Voltar");
		bVoltar.addActionListener(controller);
		bCriarHtml = new JButton("Criar HTML");
		bCriarHtml.addActionListener(controller);

		this.setBorder(BorderFactory.createTitledBorder("Menu"));
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gb = new GridBagConstraints();
		
		gb.fill = GridBagConstraints.HORIZONTAL;
		gb.weighty = 1;
		
		int i = 0, j = 0;

		for(String propriedade : propriedades) {

			gb.anchor = GridBagConstraints.EAST;
			gb.weightx = 0.2;
			gb.gridy = ++j;
			this.add(lbsPropriedades.get(j-1), gb);

			gb.anchor = GridBagConstraints.WEST;
			gb.weightx = 1;
			gb.gridx = ++i;
			this.add(entradas.get(propriedade), gb);
			gb.gridx = --i;

		}
		
		gb.gridx = 0;
		gb.gridy = ++j;
		this.add(bVoltar, gb);
		
		gb.gridx = ++i;
		this.add(bCriarHtml, gb);
	}
	
	public JButton getbCriarHtml()
	{
		return bCriarHtml;
	}

	public JButton getbVoltar()
	{
		return bVoltar;
	}
	
	public HashMap<String, JTextField> getEntradas() 
	{
		return entradas;
	}
	
	public String getNomeCard()
	{
		return nomeCard;
	}
}