package view;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class JanelaPrincipal {

	private static final int LARGURA = 400;
	private static final int ALTURA = 400; 
	    
	private JFrame janela;
	private JPanel painel;
	
	public JanelaPrincipal(JPanel painel) 
	{
		janela = new JFrame("Gerador de HTML");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setLocation(500, 300);
		janela.setPreferredSize(new Dimension(LARGURA, ALTURA));
    	janela.setVisible(true);

    	this.painel = painel;
    	
		janela.add(this.painel);
    	janela.pack();
	}
	
	public void mudarPainel(JPanel painel)
	{
		janela.remove(this.painel);
		this.painel = painel;
		janela.add(painel);
		janela.setSize(painel.getSize());
    	janela.pack();
	}
	
	public JPanel getPainel() 
	{
		return painel;
	}
}
