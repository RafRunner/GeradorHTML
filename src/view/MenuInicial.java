package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.*;


import controller.MenuInicialController;

public class MenuInicial extends JPanel {
	
	private JButton bSelecionar;
	private JLabel lSelecione;
	private JComboBox<String> cTemplates;
	private JanelaPrincipal janelaPai;

	public MenuInicial() 
	{
		this.setVisible(true);
		
		JLabel[] espacos = new JLabel[2];
		
		for(int i = 0; i < espacos.length; i++) {
			espacos[i] = new JLabel("");
		}
		
		lSelecione = new JLabel("Selecione um Template para trabalhar!");

		cTemplates = new JComboBox<>();
		cTemplates.setBorder(BorderFactory.createBevelBorder(1));
		
		bSelecionar = new JButton("Selecionar");
		bSelecionar.addActionListener(new MenuInicialController(this));
		
		this.setBorder(BorderFactory.createTitledBorder("Menu"));
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gb = new GridBagConstraints();
		
		gb.anchor = GridBagConstraints.CENTER;
		gb.fill = GridBagConstraints.HORIZONTAL;
		gb.weightx = 1;
		gb.weighty = 1;
		
		int i = 0, j = 0;
		
		gb.gridx = i;
		gb.gridy = j;
		this.add(espacos[0], gb);
		
		gb.weightx = 0.1;
		gb.weighty = 0.1;
		
		gb.gridx = ++i;
		gb.gridy = ++j;
		this.add(lSelecione, gb);
		
		gb.gridx = i;
		gb.gridy = ++j;
		this.add(cTemplates, gb);
		
		gb.gridx = i;
		gb.gridy = ++j;
		this.add(bSelecionar, gb);
		
		gb.weightx = 1;
		gb.weighty = 1;
		
		gb.gridx = ++i;
		gb.gridy = ++j;
		this.add(espacos[1], gb);
	}

	public JButton getbSelecionar() 
	{
		return bSelecionar;
	}

	public JLabel getlSelecione() 
	{
		return lSelecione;
	}
	
	public String getAbaSelecionada() 
	{
		try {
			return cTemplates.getSelectedItem().toString();
		} catch (NullPointerException ignored) {
			System.out.println(ignored.getStackTrace());
			return null;
		}
	}

	public void setcTemplates(String[] itens) 
	{
		for(String item : itens) {
			cTemplates.addItem(item);
		}
	}
}
