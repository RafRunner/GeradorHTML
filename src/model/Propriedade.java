package model;

public class Propriedade {

	public String nome;
	public String regex;
	public String valor;
	
	public Propriedade(String nome, String regex, String valor)
	{
		this.nome = nome;
		this.regex = regex;
		this.valor = valor;
	}
}
