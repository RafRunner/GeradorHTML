package model;

import java.util.ArrayList;

public interface ICard {

	public Propriedade[] getPropriedadesObrigatorias();
	public Propriedade[] getPropriedadesOpcionais();
	public ArrayList<String> getPropriedades();
	public String getPropriedade(String propriedade);
	public void setPropriedade(String propriedade, String valor);
	public String getNomeArquivoTemplate();
}
