package model;

import java.util.ArrayList;

//Fazer elementos genéricos dessa classe serem gerados em runtime
public class CardMembros implements ICard {

	private final static String NOME_ARQUIVO_TEMPLATE_CARD_MEMBROS = "res/templateCardMembros.txt";
	
	private Propriedade nome = new Propriedade("nome", "->nome", null);	
	private Propriedade frase = new Propriedade("frase", "->frase", null);
	private Propriedade imagem = new Propriedade("imagem", "->imagem", null);
	private Propriedade lotacao = new Propriedade("lotacao", "->lotacao", null);
	private Propriedade facebook = new Propriedade("facebook", "->facebook", null);
	private Propriedade linkedin = new Propriedade("linkedin", "->linkedin", null);
	private Propriedade instagram = new Propriedade("instagram", "->instagram", null);
	private Propriedade email = new Propriedade("email", "->email", null);
	
	private final Propriedade[] propriedadesObrigatorias = {
			nome, 
			frase, 
			imagem, 
			lotacao,
			email };
	
	private final Propriedade[] propriedadesOpcionais = {
			facebook, 
			linkedin, 
			instagram };

	@Override
	public String getPropriedade(String propriedade) 
	{
		switch(propriedade) {
			case "nome": return nome.valor;
			case "frase": return frase.valor;
			case "imagem": return imagem.valor;
			case "lotacao": return lotacao.valor;
			case "facebook": return facebook.valor;
			case "linkedin": return linkedin.valor;
			case "instagram": return instagram.valor;
			case "email": return email.valor;
		}
		return null;
	}

	@Override
	public Propriedade[] getPropriedadesObrigatorias()
	{
		return propriedadesObrigatorias;
	}

	@Override
	public Propriedade[] getPropriedadesOpcionais() 
	{
		return propriedadesOpcionais;
	}

	@Override
	public String getNomeArquivoTemplate() {
		return NOME_ARQUIVO_TEMPLATE_CARD_MEMBROS;
	}

	@Override
	public ArrayList<String> getPropriedades() 
	{
		ArrayList<String> propriedades = new ArrayList<String>();
		for(Propriedade propriedade : propriedadesObrigatorias) {
			propriedades.add(propriedade.nome);
		}
		for(Propriedade propriedade : propriedadesOpcionais) {
			propriedades.add(propriedade.nome);
		}
		return propriedades;
	}

	@Override
	public void setPropriedade(String propriedade, String valor) 
	{
		switch(propriedade) {
			case "nome": nome.valor = valor;
				break;
			case "frase": frase.valor = valor;
				break;
			case "imagem": imagem.valor = valor;
				break;
			case "lotacao": lotacao.valor = valor;
				break;
			case "facebook": facebook.valor = valor;
				break;
			case "linkedin": linkedin.valor = valor;
				break;
			case "instagram": instagram.valor = valor;
				break;
			case "email": email.valor = valor;
				break;
		}
	}
}
