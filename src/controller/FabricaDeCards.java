package controller;

import model.ICard;
import model.Propriedade;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import leitorArquivos.Leitor;

public class FabricaDeCards {

	public static String criaHtmlCard(String nomeArquivo, ICard card)
	{
		ArrayList<String> Html = Leitor.lerArquivo(nomeArquivo);	
		
		//Removendo atributos opcionais caso n�o forem informados
		for(Propriedade propriedadeOpcional : card.getPropriedadesOpcionais()) {
			if(propriedadeOpcional.valor.equals(null) || propriedadeOpcional.valor.equals("")) {
				Html.remove(Leitor.regexSearchLinha(Html, propriedadeOpcional.regex));
			}
		}
		
		String htmlGerado = Html.toString();
	
		for(Propriedade propriedade : card.getPropriedadesObrigatorias()) {
			Pattern p = Pattern.compile(propriedade.regex);
			Matcher matcher = p.matcher(htmlGerado);
			htmlGerado = matcher.replaceAll(propriedade.valor);
			System.out.println(htmlGerado);
		}
		for(Propriedade propriedade : card.getPropriedadesOpcionais()) {
			if(!propriedade.valor.equals(null) || propriedade.valor.equals("")) {
				Pattern p = Pattern.compile(propriedade.regex);
				Matcher matcher = p.matcher(htmlGerado);
				htmlGerado = matcher.replaceAll(propriedade.valor);
			}
		}
		
		return htmlGerado.replace(",", "\n").replace("[", "").replace("]", "");
	}
}
