package controller;

import model.ICard;

public class CardController {

	private static String[] cardsExistentes = {
			"CardMembros"
	};
	
	public static String[] getCardsExistentes()
	{
		return cardsExistentes;
	}
	
	public static ICard getClasseCard(String nomeClasseCard)
	{
		try {
		    Class<?> classe = Class.forName("model." + nomeClasseCard);
		    return (ICard) classe.newInstance();
		    
		 } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
		        e.printStackTrace();
				return null;
		}
	}
}
