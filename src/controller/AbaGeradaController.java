package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import model.ICard;
import view.AbaGerada;
import view.JanelaPrincipal;
import view.MenuInicial;

public class AbaGeradaController implements ActionListener {

	private AbaGerada aba;
	private JanelaPrincipal janelaPrincipal;
	
	public AbaGeradaController(AbaGerada aba) 
	{
		this.aba = aba;
		janelaPrincipal = JanelaPrincipalController.getJanelaPrincipal();
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource().equals(aba.getbVoltar())) {
			MenuInicial novoMenu = new MenuInicial();
			janelaPrincipal.mudarPainel(novoMenu);
		}
		else {
			ICard cardASerCriado = CardController.getClasseCard(aba.getNomeCard());
			
			for(String propriedade : aba.getEntradas().keySet()) {
				cardASerCriado.setPropriedade(propriedade, aba.getEntradas().get(propriedade).getText());
			}
			for(String propriedade : aba.getEntradas().keySet()) {
				System.out.println(propriedade + ", " + aba.getEntradas().get(propriedade).getText());
			}
			
			String Html = FabricaDeCards.criaHtmlCard(cardASerCriado.getNomeArquivoTemplate(), cardASerCriado);
			JTextArea ta = new JTextArea(20, 100);
			ta.setText(Html);
			ta.setWrapStyleWord(true);
            ta.setLineWrap(true);
            ta.setCaretPosition(0);
			JOptionPane.showMessageDialog(null, new JScrollPane(ta), "Resultado", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
