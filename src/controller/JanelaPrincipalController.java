package controller;

import view.JanelaPrincipal;
import view.MenuInicial;

public class JanelaPrincipalController {

	private static JanelaPrincipal janelaPrincipal;

	public static void main(String[] args)
	{
		MenuInicial menu = new MenuInicial();
		JanelaPrincipal janela = new JanelaPrincipal(menu);
		setJanela(janela);
	}

	private static void setJanela(JanelaPrincipal janela) {
		janelaPrincipal = janela;
	}

	protected static JanelaPrincipal getJanelaPrincipal() {
		return janelaPrincipal;
	}
}
