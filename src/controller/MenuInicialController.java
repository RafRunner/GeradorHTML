package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.ICard;
import view.AbaGerada;
import view.JanelaPrincipal;
import view.MenuInicial;

public class MenuInicialController implements ActionListener {

	private JanelaPrincipal janela;
	private  MenuInicial menu;
	
	public MenuInicialController(MenuInicial menu) 
	{
		this.menu = menu;
		String[] cardsExistentes = CardController.getCardsExistentes();
		this.menu.setcTemplates(cardsExistentes);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource().equals(menu.getbSelecionar())) {
			janela = JanelaPrincipalController.getJanelaPrincipal();
			String nomeAbaSelecionada = menu.getAbaSelecionada();
			ICard cardSelecionado = CardController.getClasseCard(nomeAbaSelecionada);
			AbaGerada aba = new AbaGerada(cardSelecionado.getPropriedades(), nomeAbaSelecionada);
			janela.mudarPainel(aba);
		}
	}

	public MenuInicial getMenu() {
		return menu;
	}
}
