<div class="col l3 m6 s12">
      <div class="lt-card card z-depth-0">
         <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="{{ 'assets/img/pages/about/->imagem'|theme }}">
         </div>
         <div class="card-content">
            <span class="lt-card-title card-title activator grey-text text-darken-4">->nome</span>
            <p class="lt-card-subtitle">->lotacao</p>
         </div>
         <div class="card-reveal">
            <span class="lt-card-title card-title grey-text text-darken-4">->nome<i class="material-icons right">close</i></span>
            <p>->frase</p>
            <p class="lt-card-social-buttons">
               <a target="_blank" href="->facebook"><i class="lt-card-social-button blue-text text-darken-4 fa fa-facebook-square"></i></a>
               <a target="_blank" href="->linkedin"><i class="lt-card-social-button blue-text fa fa-linkedin-square"></i></a>
               <a target="_blank" href="->instagram"><i class="lt-card-social-button red-text fa fa-instagram"></i></a>
               <a href="mailto:->email"><i class="lt-card-social-button red-text fa fa-envelope"></i></a>
            </p>
         </div>
      </div>
   </div>